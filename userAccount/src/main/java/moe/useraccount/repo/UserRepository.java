package moe.useraccount.repo;

import moe.useraccount.pojo.useraccounts;
import org.springframework.data.jpa.repository.JpaRepository;



public interface UserRepository  extends JpaRepository<useraccounts,Long> {

}
