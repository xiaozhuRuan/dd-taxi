# dd-taxi

#### 介绍

北邮Web后端开发大作业
打车的都是DD，开车的也是DD，谁知道写代码的是不是DD呢（

#### 软件架构

![](https://images.gitee.com/uploads/images/2021/0702/154552_bf7ec2a1_4986363.png "uml.png")

#### 安装教程

跑就完事了x

#### 使用说明

1. 源码中的底层服务服务器IP地址是写死的，运行需要修改yml中的默认IP地址或者使用配置参数CONFIG_HOST和REGISTRY_HOST
2. 数据库服务器是代码里写死的，账号密码并非隐私相关，可以任意修改

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
