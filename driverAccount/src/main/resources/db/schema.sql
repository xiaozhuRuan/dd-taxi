create database if not exists accounts;
use accounts;
alter database accounts
    default character set utf8
    default collate utf8_general_ci;

drop table if exists driverAccounts;

create table if not exists driverAccounts
(
    id  bigint not null AUTO_INCREMENT primary key ,
    name nvarchar(64) not null ,
    type tinyint not null ,
    password nvarchar(64) not null ,
    phone nvarchar(64) not null unique ,
    license nvarchar(64) not null ,
    cardetail nvarchar(200) ,
    finished int unsigned ,
    rating float
) engine = InnoDB;
