package moe.driveraccount.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "driveraccounts")
public class DriverAccount {
    @Id
    @GeneratedValue
    private Long id;
    private String name;
    private int type;
    private String password;
    @Column(unique = true)
    private String phone;
    private String license;
    @Column(name = "cardetail")
    private String carDetail;
    private int finished;
    private float rating;
}
