package moe.driveraccount.repository;

import moe.driveraccount.entity.DriverAccount;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DriverRepository extends JpaRepository<DriverAccount, Long> {
    DriverAccount findByPhone(String phone);
}
