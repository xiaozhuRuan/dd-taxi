package moe.driveraccount.controller;

import moe.driveraccount.entity.DriverAccount;
import moe.driveraccount.repository.DriverRepository;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/restful/driver")
public class DriverController {
    private final DriverRepository driverRepository;

    public DriverController(DriverRepository driverRepository) {
        this.driverRepository = driverRepository;
    }

    @PostMapping("/reg")
    public ResponseEntity<Void> reg(
            @RequestParam(name = "name") String name,
            @RequestParam(name = "password") String password,
            @RequestParam(name = "phone") String phone,
            @RequestParam(name = "license") String license,
            @RequestParam(name = "carDetail") String carDetail) {
        DriverAccount driverAccount = new DriverAccount();

        driverAccount.setName(name);
        driverAccount.setType(1);
        driverAccount.setPassword(password);
        driverAccount.setPhone(phone);
        driverAccount.setLicense(license);
        driverAccount.setCarDetail(carDetail);
        driverAccount.setFinished(0);
        driverAccount.setRating(5);

        System.out.println(driverAccount);

        try {
            driverRepository.save(driverAccount);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }

        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PostMapping("/login")
    public ResponseEntity<String> login(
            @RequestParam(name = "phone") String phone,
            @RequestParam(name = "password") String password) {
        DriverAccount driverAccount = driverRepository.findByPhone(phone);
        if (driverAccount == null)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        if (!driverAccount.getPassword().equals(password))
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("id", driverAccount.getId());
        jsonObject.put("name", driverAccount.getName());
        jsonObject.put("type", driverAccount.getType());
        jsonObject.put("phone", driverAccount.getPhone());
        jsonObject.put("license", driverAccount.getLicense());
        jsonObject.put("carDetail", driverAccount.getCarDetail());
        jsonObject.put("finished", driverAccount.getFinished());
        jsonObject.put("rating", driverAccount.getRating());

        return new ResponseEntity<>(jsonObject.toString(), HttpStatus.OK);
    }

    @PostMapping("/rating")
    public ResponseEntity<Float> rating(
            @RequestParam(name = "id") Long id,
            @RequestParam(name = "rating") float rating) {
        DriverAccount driverAccount = driverRepository.findById(id).orElse(null);
        if (driverAccount == null)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        int finished = driverAccount.getFinished();
        float oldRating = driverAccount.getRating();

        float newRating = oldRating * finished + rating;
        finished++;
        newRating = newRating / finished;
        driverAccount.setRating(newRating);

        driverRepository.save(driverAccount);

        return new ResponseEntity<>(newRating, HttpStatus.OK);
    }
}
