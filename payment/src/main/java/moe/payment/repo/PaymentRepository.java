package moe.payment.repo;

import java.util.List;

import moe.payment.entity.payment;
import org.springframework.data.jpa.repository.JpaRepository;


public interface PaymentRepository extends JpaRepository<payment,Long> {
    List<payment> findAllByUseridAndStatusIsNotAndStatusIsNot(Long userid, int status, int status2);
    List<payment> findAllByStartposxIsBetweenAndStartposyIsBetweenAndStatus(float x1, float x2, float y1, float y2, int status);
    payment findAllById(Long id);
}
