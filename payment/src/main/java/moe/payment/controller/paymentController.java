package moe.payment.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;

import moe.payment.common.Result;
import moe.payment.entity.payment;
import moe.payment.repo.PaymentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Configuration
@Tag(name = "paymentAPI",description = "payment的controller接口。")
@RestController
public class paymentController {
    @Autowired

    PaymentRepository paymentRepository;
    public paymentController(PaymentRepository paymentRepository){
        this.paymentRepository=paymentRepository;
    }
    public static int PRICE = 1;
    @Operation(method = "GET",description = "创建一个新订单。")
    @GetMapping("/restful/payment/new")
    public Result<?> register(@Parameter Long userID, @Parameter float startPos_x,@Parameter float startPos_y, @Parameter float endPos_x,@Parameter float endPos_y) {

        List<payment> mylist=new ArrayList<payment>();
        mylist=paymentRepository.findAllByUseridAndStatusIsNotAndStatusIsNot(userID,5,0);
        if(!mylist.isEmpty()){
            return Result.error(501,"当前已有订单，不能再次预约");
        }
        Date nowdate=new Date();
        payment temp=new payment();
        float distance;
        float price;
        temp.setUserid(userID);
        temp.setStartposx(startPos_x);
        temp.setStartposy(startPos_y);
        temp.setEndposx(endPos_x);
        temp.setEndposy(endPos_y);
        distance=Math.abs(startPos_x+startPos_y-endPos_x-endPos_y);
        price=distance/PRICE;
        temp.setDistance(distance);
        temp.setPrice(price);
        temp.setStatus(1);
        temp.setStarttime(nowdate.toString());
        paymentRepository.save(temp);
        return Result.ok(temp);
    }
    @Operation(method = "GET",description = "获取当前订单信息")
    @GetMapping("/restful/payment/mypaymentnow")
    public Result<?> mypayment(@Parameter Long userID){
        List<payment> mylist=new ArrayList<payment>();
        mylist=paymentRepository.findAllByUseridAndStatusIsNotAndStatusIsNot(userID,5,0);
        if(mylist.isEmpty()){
            return Result.error(501,"当前没有订单");
        }
        else{
            payment temp=new payment();
            temp=mylist.get(0);
            return Result.ok(temp);
        }
    }

    @Operation(method = "GET",description = "司机获取当前可接单的信息")
    @GetMapping("/restful/payment/driverpayment")
    public Result<?> driverpayment(@Parameter float dPos_x,@Parameter float dPos_y){
        List<payment> dpayment=new ArrayList<payment>();
        dpayment=paymentRepository.findAllByStartposxIsBetweenAndStartposyIsBetweenAndStatus(dPos_x-10,dPos_x+10,dPos_y-10,dPos_y+10,1);
        if(dpayment.isEmpty()){
            return Result.error(501,"当前附近没有可接取订单");
        }
        else{
            return Result.ok(dpayment);
        }
    }
    @Operation(method = "GET",description = "司机接取订单")
    @GetMapping("/restful/payment/driveraccess")
    public Result<?> driveraccess(@Parameter Long driverID, @Parameter Long id,@Parameter float dPos_x,@Parameter float dPos_y){
        payment temp=new payment();
        temp=paymentRepository.findAllById(id);
        if(temp.getStatus()!=1){
            return Result.error(501,"当前单的状态不可接单");
        }
        temp.setDriverid(driverID);
        temp.setDriverposx(dPos_x);
        temp.setDriverposy(dPos_y);
        temp.setStatus(2);
        paymentRepository.save(temp);
        temp=paymentRepository.findAllById(id);
        return Result.ok(temp);
    }
    @Operation(method = "GET",description = "开始上路")
    @GetMapping("/restful/payment/travelstart")
    public Result<?> travelstart(@Parameter Long id){
        payment temp=new payment();
        temp=paymentRepository.findAllById(id);
        if(temp.getStatus()==2){
            temp.setStatus(3);
            paymentRepository.save(temp);
            temp=paymentRepository.findAllById(id);
            return Result.ok(temp);
        }
        else{
            return Result.error(501,"状态错误");
        }
    }

    @Operation(method = "GET",description = "订单进入等待支付")
    @GetMapping("/restful/payment/waitToPay")
    public Result<?> waitToPay(@Parameter Long id){
        payment temp=new payment();
        temp=paymentRepository.findAllById(id);
        if(temp.getStatus()==3){
            temp.setStatus(4);
            paymentRepository.save(temp);
            temp=paymentRepository.findAllById(id);
            return Result.ok(temp);
        }
        else{
            return Result.error(501,"状态错误");
        }
    }

    @Operation(method = "GET",description = "订单已完成")
    @GetMapping("/restful/payment/finish")
    public Result<?> finish(@Parameter Long id){
        payment temp=new payment();
        temp=paymentRepository.findAllById(id);
        Date nowdate=new Date();
        if(temp.getStatus()==4){
            temp.setStatus(5);
            paymentRepository.save(temp);
            temp=paymentRepository.findAllById(id);
            temp.setEndtime(nowdate.toString());
            return Result.ok(temp);
        }
        else{
            return Result.error(501,"状态错误");
        }
    }

    @Operation(method = "GET",description = "取消订单。")
    @GetMapping("/restful/payment/mypayment/cancel")
    public Result<?> mypaymentcancel(@Parameter Long id){
        payment temp=new payment();
        temp=paymentRepository.findAllById(id);
        if (temp.getStatus()==1){
            temp.setStatus(0);
            paymentRepository.save(temp);
            return Result.ok("订单取消成功");
        }
        else if (temp.getStatus()>1&&temp.getStatus()<=5){
            return Result.error(501,"当前订单已不可取消");
        }
        else{
            return Result.error(502,"状态错误");
        }
    }

    @Operation(method = "GET",description = "对订单进行评价。")
    @GetMapping("/restful/payment/mypayment/rating")
    public Result<?> rating(@Parameter Long id,@Parameter int rating){
        payment temp=new payment();
        temp=paymentRepository.findAllById(id);
        if (temp.getStatus()!=1&&temp.getStatus()!=0){
            temp.setRating(rating);
            paymentRepository.save(temp);
            temp=paymentRepository.findAllById(id);
            return Result.ok(temp);
        }
        else{
            return Result.error(502,"状态错误");
        }
    }



}
