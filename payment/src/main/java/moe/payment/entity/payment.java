package moe.payment.entity;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.sql.Time;

@Data
@Entity
@Getter
@Setter
public class payment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private int status;
    private Long userid;
    private Long driverid;
    private float distance;
    private int rating;
    private float price;
    private float driverposx;
    private float driverposy;
    private float startposx;
    private float startposy;
    private float endposx;
    private float endposy;
    private String starttime;
    private String endtime;
}
